package net.therap.todo.controller;

import net.therap.todo.domain.User;
import net.therap.todo.helper.AuthenticationHelper;
import net.therap.todo.service.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static java.util.Collections.singletonList;

/**
 * @author tanmoy.das
 * @since 10/11/21
 */
public class LoginServlet extends HttpServlet {

    private UserService userService;

    private AuthenticationHelper authenticationHelper;

    public LoginServlet() {
        super();
        this.userService = new UserService();
        this.authenticationHelper = new AuthenticationHelper();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/views/login.jsp");
        requestDispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();

        String email = request.getParameter("email");
        String password = request.getParameter("password");

        if (!authenticationHelper.isValidCredential(email, password)) {
            request.setAttribute("errors", singletonList("Email or Password incorrect"));

            RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/views/login.jsp");
            requestDispatcher.forward(request, response);
        } else {
            User user = userService.findByEmail(email);
            session.setAttribute("SESSION_USER", user);

            response.sendRedirect("/dashboard");
        }
    }
}
