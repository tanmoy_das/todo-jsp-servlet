package net.therap.todo.controller;

import net.therap.todo.domain.User;
import net.therap.todo.service.UserService;
import net.therap.todo.util.HashGenerationUtil;
import net.therap.todo.validator.UserValidator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author tanmoy.das
 * @since 10/11/21
 */
public class RegisterServlet extends HttpServlet {

    private UserService userService;
    private UserValidator userValidator;


    public RegisterServlet() {
        super();
        this.userService = new UserService();
        this.userValidator = new UserValidator();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/views/register.jsp");
        requestDispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String email = request.getParameter("email");
        String password = request.getParameter("password");

        User user = new User(firstName, lastName, email, HashGenerationUtil.getMd5(password));

        List<String> errors = new ArrayList<>();
        userValidator.validate(user, errors);

        if (!errors.isEmpty()) {
            request.setAttribute("errors", errors);

            RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/views/register.jsp");
            requestDispatcher.forward(request, response);
        } else {

            userService.saveOrUpdate(user);
            response.sendRedirect("/login");
        }
    }
}
