package net.therap.todo.controller;

import net.therap.todo.domain.Todo;
import net.therap.todo.domain.User;
import net.therap.todo.helper.TodoHelper;
import net.therap.todo.service.TodoService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author tanmoy.das
 * @since 10/11/21
 */
public class TodoServlet extends HttpServlet {

    private TodoHelper todoHelper;

    private TodoService todoService;

    public TodoServlet() {
        super();
        this.todoHelper = new TodoHelper();
        this.todoService = new TodoService();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String action = request.getParameter("action");
        switch (action) {
            case "list":
                listTodos(request, response);
                break;
            case "create":
                create(request, response);
                break;
            case "view":
                show(request, response);
                break;
            case "edit":
                edit(request, response);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + action);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String action = getAction(request);
        switch (action) {
            case "save":
                save(request, response);
                break;
            case "update":
                update(request, response);
                break;
            case "delete":
                delete(request, response);
                break;
            case "markCompleted":
                markCompleted(request, response);
                break;
            case "markIncomplete":
                markIncomplete(request, response);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + action);
        }
    }

    private void listTodos(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        User user = (User) request.getSession().getAttribute("SESSION_USER");
        List<Todo> todos = todoService.findByCreatedBy(user);

        List<Todo> completedTodos = todos.stream().filter(Todo::isCompleted).collect(Collectors.toList());
        List<Todo> incompleteTodos = todos.stream().filter(todo -> !todo.isCompleted()).collect(Collectors.toList());

        request.setAttribute("completedTodos", completedTodos);
        request.setAttribute("incompleteTodos", incompleteTodos);

        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/views/todoList.jsp");
        requestDispatcher.forward(request, response);
    }

    private void create(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Todo todo = new Todo();
        request.setAttribute("todo", todo);

        request.setAttribute("isNew", todo.isNew());
        request.setAttribute("isEditable", true);

        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/views/todo.jsp");
        requestDispatcher.forward(request, response);
    }

    private void show(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int id = Integer.parseInt(request.getParameter("id"));
        Todo todo = todoService.find(id);

        User user = (User) request.getSession().getAttribute("SESSION_USER");
        todoHelper.checkAccess(todo, user);

        request.setAttribute("todo", todo);

        request.setAttribute("isNew", todo.isNew());
        request.setAttribute("isEditable", false);

        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/views/todo.jsp");
        requestDispatcher.forward(request, response);
    }

    private void edit(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int id = Integer.parseInt(request.getParameter("id"));
        Todo todo = todoService.find(id);
        request.setAttribute("todo", todo);

        User user = (User) request.getSession().getAttribute("SESSION_USER");
        todoHelper.checkAccess(todo, user);

        request.setAttribute("isNew", todo.isNew());
        request.setAttribute("isEditable", true);

        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/views/todo.jsp");
        requestDispatcher.forward(request, response);
    }

    private void save(HttpServletRequest request, HttpServletResponse response)
            throws IOException {

        User user = (User) request.getSession().getAttribute("SESSION_USER");
        String title = request.getParameter("title");
        String description = request.getParameter("description");

        Todo todo = new Todo(title, description, user);
        todoService.saveOrUpdate(todo);

        response.sendRedirect("/done");
    }

    private void update(HttpServletRequest request, HttpServletResponse response)
            throws IOException {

        int id = Integer.parseInt(request.getParameter("id"));
        String title = request.getParameter("title");
        String description = request.getParameter("description");

        Todo todo = todoService.find(id);

        User user = (User) request.getSession().getAttribute("SESSION_USER");
        todoHelper.checkAccess(todo, user);

        todo.setTitle(title);
        todo.setDescription(description);

        todoService.saveOrUpdate(todo);

        response.sendRedirect("/done");
    }

    private void delete(HttpServletRequest request, HttpServletResponse response)
            throws IOException {

        int id = Integer.parseInt(request.getParameter("id"));
        Todo todo = todoService.find(id);

        User user = (User) request.getSession().getAttribute("SESSION_USER");
        todoHelper.checkAccess(todo, user);

        todoService.delete(todo);

        response.sendRedirect("/done");
    }

    private void markCompleted(HttpServletRequest request, HttpServletResponse response)
            throws IOException {

        int id = Integer.parseInt(request.getParameter("id"));
        Todo todo = todoService.find(id);

        User user = (User) request.getSession().getAttribute("SESSION_USER");
        todoHelper.checkAccess(todo, user);

        todo.setCompleted(true);
        todoService.saveOrUpdate(todo);

        response.sendRedirect("/done");
    }

    private void markIncomplete(HttpServletRequest request, HttpServletResponse response)
            throws IOException {

        int id = Integer.parseInt(request.getParameter("id"));
        Todo todo = todoService.find(id);

        User user = (User) request.getSession().getAttribute("SESSION_USER");
        todoHelper.checkAccess(todo, user);

        todo.setCompleted(false);
        todoService.saveOrUpdate(todo);

        response.sendRedirect("/done");
    }

    private String getAction(HttpServletRequest request) {
        if (request.getParameter("_action_save") != null) {
            return "save";
        }

        if (request.getParameter("_action_update") != null) {
            return "update";
        }

        if (request.getParameter("_action_delete") != null) {
            return "delete";
        }

        if (request.getParameter("_action_mark_completed") != null) {
            return "markCompleted";
        }

        if (request.getParameter("_action_mark_incomplete") != null) {
            return "markIncomplete";
        }

        return "";
    }
}
