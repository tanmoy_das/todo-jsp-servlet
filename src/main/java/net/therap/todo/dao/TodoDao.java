package net.therap.todo.dao;

import net.therap.todo.domain.Todo;
import net.therap.todo.domain.User;
import net.therap.todo.util.ConnectionManager;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * @author tanmoy.das
 * @since 10/11/21
 */
public class TodoDao {

    private final EntityManager em;

    public TodoDao() {
        this.em = ConnectionManager.initConnection();
    }

    public Todo find(int id) {
        return em.find(Todo.class, id);
    }

    public List<Todo> findByCreatedBy(User user) {

        return em.createNamedQuery("Todo.findByCreatedBy", Todo.class)
                .setParameter("createdBy", user)
                .getResultList();
    }

    public void saveOrUpdate(Todo todo) {
        em.getTransaction().begin();

        if (todo.isNew()) {
            em.persist(todo);
        } else {
            em.merge(todo);
        }

        em.getTransaction().commit();
    }

    public void delete(Todo todo) {
        em.getTransaction().begin();
        em.remove(todo);
        em.getTransaction().commit();
    }
}
