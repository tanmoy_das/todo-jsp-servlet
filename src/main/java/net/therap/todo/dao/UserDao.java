package net.therap.todo.dao;

import net.therap.todo.domain.User;
import net.therap.todo.util.ConnectionManager;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * @author tanmoy.das
 * @since 10/11/21
 */
public class UserDao {

    private final EntityManager em;

    public UserDao() {
        this.em = ConnectionManager.initConnection();
    }

    public User find(int id) {
        return em.find(User.class, id);
    }

    public User findByEmail(String email) {

        return em.createNamedQuery("User.findByEmail", User.class)
                .setParameter("email", email)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    public List<User> findAll() {
        TypedQuery<User> typedQuery = em.createNamedQuery("User.findAll", User.class);

        return typedQuery.getResultList();
    }

    public void saveOrUpdate(User user) {
        em.getTransaction().begin();

        if (user.isNew()) {
            em.persist(user);
        } else {
            em.merge(user);
        }

        em.getTransaction().commit();
    }

    public void delete(User user) {
        em.getTransaction().begin();
        em.remove(user);
        em.getTransaction().commit();
    }
}
