package net.therap.todo.exception;

/**
 * @author tanmoy.das
 * @since 10/17/21
 */
public class WebException extends RuntimeException {

    public WebException(String message) {
        super(message);
    }
}
