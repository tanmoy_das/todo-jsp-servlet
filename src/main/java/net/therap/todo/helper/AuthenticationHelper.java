package net.therap.todo.helper;

import net.therap.todo.domain.User;
import net.therap.todo.service.UserService;
import net.therap.todo.util.HashGenerationUtil;

/**
 * @author tanmoy.das
 * @since 10/11/21
 */
public class AuthenticationHelper {

    private UserService userService;

    public AuthenticationHelper() {
        this.userService = new UserService();
    }

    public boolean isValidCredential(String email, String inputPassword) {
        User user = userService.findByEmail(email);
        if (user == null) {
            return false;
        }

        String hashInput = HashGenerationUtil.getMd5(inputPassword);
        return hashInput.equals(user.getPassword());
    }
}
