package net.therap.todo.helper;

import net.therap.todo.domain.Todo;
import net.therap.todo.domain.User;
import net.therap.todo.exception.WebException;

/**
 * @author tanmoy.das
 * @since 10/17/21
 */
public class TodoHelper {

    public void checkAccess(Todo todo, User user) {
        if (!todo.getCreatedBy().equals(user)) {
            throw new WebException("User doesn't have access to this todo");
        }
    }
}
