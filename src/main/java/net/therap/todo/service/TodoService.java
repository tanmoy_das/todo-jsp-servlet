package net.therap.todo.service;

import net.therap.todo.dao.TodoDao;
import net.therap.todo.domain.Todo;
import net.therap.todo.domain.User;

import java.util.List;

/**
 * @author tanmoy.das
 * @since 10/11/21
 */
public class TodoService {

    private final TodoDao todoDao;

    public TodoService() {
        this.todoDao = new TodoDao();
    }

    public Todo find(int id) {
        return todoDao.find(id);
    }

    public List<Todo> findByCreatedBy(User user) {
        return todoDao.findByCreatedBy(user);
    }

    public void saveOrUpdate(Todo todo) {
        todoDao.saveOrUpdate(todo);
    }

    public void delete(Todo todo) {
        todoDao.delete(todo);
    }
}

