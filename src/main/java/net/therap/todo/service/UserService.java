package net.therap.todo.service;

import net.therap.todo.dao.UserDao;
import net.therap.todo.domain.User;

/**
 * @author tanmoy.das
 * @since 10/11/21
 */
public class UserService {

    private final UserDao userDao;

    public UserService() {
        this.userDao = new UserDao();
    }

    public User findByEmail(String email) {
        return userDao.findByEmail(email);
    }

    public void saveOrUpdate(User user) {
        userDao.saveOrUpdate(user);
    }

}

