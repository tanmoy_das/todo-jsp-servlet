package net.therap.todo.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * @author tanmoy.das
 * @since 10/11/21
 */
public class ConnectionManager {

    public static EntityManager initConnection() {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("todo_app");

        return entityManagerFactory.createEntityManager();
    }
}
