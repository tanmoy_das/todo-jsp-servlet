package net.therap.todo.validator;

import net.therap.todo.domain.User;
import net.therap.todo.service.UserService;

import java.util.List;

/**
 * @author tanmoy.das
 * @since 10/11/21
 */
public class UserValidator implements Validator<User> {

    private UserService userService;

    public UserValidator() {
        this.userService = new UserService();
    }

    @Override
    public void validate(User user, List<String> errors) {

        if (user.getFirstName().length() < 2 || user.getFirstName().length() > 20) {
            errors.add("First Name length must be between 2 and 20");
        }

        if (user.getLastName().length() < 2 || user.getLastName().length() > 20) {
            errors.add("Last Name length must be between 2 and 20");
        }

        if (hasDuplicateEmail(user)) {
            errors.add("Another user with this email already exists");
        }
    }

    private boolean hasDuplicateEmail(User user) {
        User persistedUser = userService.findByEmail(user.getEmail());

        return (user.isNew() && persistedUser != null)
                || (!user.isNew() && persistedUser != null && persistedUser.getId() != user.getId());
    }
}
