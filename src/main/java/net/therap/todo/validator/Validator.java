package net.therap.todo.validator;

import java.util.List;

/**
 * @author tanmoy.das
 * @since 10/11/21
 */
public interface Validator<T> {

    void validate(T target, List<String> errors);

}
