CREATE TABLE todo_app.user
(
    id             int              NOT NULL AUTO_INCREMENT,
    first_name     varchar(100)     NOT NULL,
    last_name      varchar(100)     NOT NULL,
    email          varchar(100)     NOT NULL,
    password       varchar(255)     NOT NULL,
    version        int              NOT NULL DEFAULT 0,
    PRIMARY KEY (id),
    UNIQUE KEY uk_email (email)
);

CREATE TABLE todo_app.todo
(
    id                int              NOT NULL AUTO_INCREMENT,
    title             varchar(100)     NOT NULL,
    description       varchar(100)     NOT NULL,
    completed         tinyint(1)       NOT NULL DEFAULT 0,
    created_by_id     int              NOT NULL,
    version           int              NOT NULL DEFAULT 0,
    PRIMARY KEY (id),
    FOREIGN KEY fk_created_by (created_by_id) REFERENCES user (id)
);
