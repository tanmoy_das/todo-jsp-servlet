<%--
  @author tanmoy.das
  @since 10/11/21
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Todo App</title>

    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>

<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
    <h5 class="my-0 mr-md-auto font-weight-normal">Todo App</h5>
    <nav class="my-2 my-md-0 mr-md-3">
        <c:if test="${not empty user}">
            <a class="p-2 text-dark" href="/dashboard">Dashboard</a>
            <a class="p-2 text-dark" href="/profile">Profile</a>
        </c:if>

        <c:if test="${empty user}">
            <a class="p-2 text-dark" href="/">Home</a>
        </c:if>
    </nav>

    <c:if test="${not empty user}">
        <a class="btn btn-outline-danger" href="/logout">Logout</a>
    </c:if>

    <c:if test="${empty user}">
        <a class="btn btn-outline-primary" href="/login">Login</a>
    </c:if>
</div>

<div class="container">
    <div class="m-3 d-flex justify-content-center">
        <c:choose>
            <c:when test="${not empty message}">
                <div class="alert alert-danger">
                    <c:out value="${message}"/>
                </div>
            </c:when>
            <c:otherwise>
                <div class="alert alert-danger">
                    App Error occurred
                </div>
            </c:otherwise>
        </c:choose>
    </div>
</div>

</body>
</html>