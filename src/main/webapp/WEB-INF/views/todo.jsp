<%--
  @author tanmoy.das
  @since 10/11/21
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Todo App :: Todo</title>

    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>

<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
    <h5 class="my-0 mr-md-auto font-weight-normal">Todo App</h5>
    <nav class="my-2 my-md-0 mr-md-3">
        <a class="p-2 text-dark" href="/dashboard">Dashboard</a>
        <a class="p-2 text-dark" href="/profile">Profile</a>
    </nav>
    <a class="btn btn-outline-danger" href="/logout">Logout</a>
</div>

<div class="container">
    <div class="mb-3 d-flex justify-content-center">
        <div class="col-12">
            <form action="/todo" method="post">

                <c:if test="${not empty errors}">
                    <div class="my-3 p-2 alert alert-danger">
                        <c:forEach var="error" items="${errors}">
                            <div><c:out value="${error}"/></div>
                        </c:forEach>
                    </div>
                </c:if>

                <c:if test="${not isNew}">
                    <input name="id" type="hidden" value="${todo.id}">
                </c:if>

                <div class="form-group">
                    <label for="title">Title</label>

                    <c:if test="${not isEditable}">
                        <input type="text" class="form-control" name="title" id="title"
                               value="${todo.title}" disabled required>
                    </c:if>

                    <c:if test="${isEditable}">
                        <input type="text" class="form-control" name="title" id="title"
                               value="${todo.title}" required>
                    </c:if>
                </div>

                <div class="form-group">
                    <label for="description">Description</label>

                    <c:if test="${not isEditable}">
                        <textarea class="form-control" name="description" id="description"
                                  rows="10" disabled required>${todo.description}</textarea>
                    </c:if>

                    <c:if test="${isEditable}">
                        <textarea class="form-control" name="description" id="description"
                                  rows="10" required>${todo.description}</textarea>
                    </c:if>
                </div>

                <div class="card text-right">
                    <div class="card-body">
                        <c:if test="${isNew}">
                            <button class="btn btn-outline-primary" name="_action_save">Save</button>
                        </c:if>

                        <c:if test="${not isNew and isEditable}">
                            <button class="btn btn-outline-primary" name="_action_update">Update</button>
                        </c:if>

                        <c:if test="${not isNew and not isEditable}">
                            <c:url var="todoEditUrl" value="/todo">
                                <c:param name="action" value="edit"/>
                                <c:param name="id" value="${todo.id}"/>
                            </c:url>

                            <c:if test="${not todo.isCompleted()}">
                                <button class="btn btn-outline-info" name="_action_mark_completed">
                                    Mark as Completed
                                </button>
                            </c:if>

                            <c:if test="${todo.isCompleted()}">
                                <button class="btn btn-outline-info" name="_action_mark_incomplete">
                                    Mark as Incomplete
                                </button>
                            </c:if>

                            <button type="button"
                                    class="btn btn-outline-primary"
                                    onclick="window.location='${todoEditUrl}'">
                                Edit
                            </button>

                            <button class="btn btn-outline-danger" name="_action_delete">Delete</button>
                        </c:if>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

</body>
</html>