<%--
  @author tanmoy.das
  @since 10/11/21
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Todo App :: Todo List</title>

    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>

<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
    <h5 class="my-0 mr-md-auto font-weight-normal">Todo App</h5>
    <nav class="my-2 my-md-0 mr-md-3">
        <a class="p-2 text-dark" href="/dashboard">Dashboard</a>
        <a class="p-2 text-dark" href="/profile">Profile</a>
    </nav>
    <a class="btn btn-outline-danger" href="/logout">Logout</a>
</div>

<div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
    <h1 class="display-4">Todo App</h1>
    <p class="lead">Keep track of what you need to do, from the comfort of your home</p>
</div>

<div class="container">
    <div class="m-3">
        <div class="text-center font-weight-bold m-3">Incomplete Todos</div>

        <table class="table table-striped col-sm-12 col-md-10 col-lg-8">
            <thead>
            <tr>
                <th class="text-secondary col-8">Title</th>
                <th class="text-secondary col-4">Action</th>
            </tr>
            </thead>

            <tbody>
            <c:if test="${empty incompleteTodos}">
                <td colspan="2">No incomplete todo is available yet</td>
            </c:if>

            <c:forEach var="todo" items="${incompleteTodos}">
                <tr>
                    <c:url var="todoShowUrl" value="/todo">
                        <c:param name="action" value="view"/>
                        <c:param name="id" value="${todo.id}"/>
                    </c:url>

                    <td class="col-8">${todo.title}</td>
                    <td class="col-4"><a href="${todoShowUrl}">Show</a></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>

        <br><br>

        <div class="text-center font-weight-bold m-3">Completed Todos</div>

        <table class="table table-striped col-sm-12 col-md-10 col-lg-8">
            <thead>
            <tr>
                <th class="text-secondary col-8">Title</th>
                <th class="text-secondary col-4">Action</th>
            </tr>
            </thead>

            <tbody>
            <c:if test="${empty completedTodos}">
                <td colspan="2">No completed todo is available yet</td>
            </c:if>

            <c:forEach var="todo" items="${completedTodos}">
                <tr>
                    <c:url var="todoShowUrl" value="/todo">
                        <c:param name="action" value="view"/>
                        <c:param name="id" value="${todo.id}"/>
                    </c:url>

                    <td class="col-8">${todo.title}</td>
                    <td class="col-4"><a href="${todoShowUrl}">Show</a></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>

</body>
</html>